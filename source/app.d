import core.thread;
import std.array;
import std.algorithm;
import std.exception;
import std.conv;
import std.range;
import std.stdio;
import std.traits;
import std.typecons;

import darg;
import ggplotd.axes;
import ggplotd.geom;
import ggplotd.ggplotd;
import ggplotd.gtk;
import ggplotd.legend;

enum kVersion = "0.2.0";

struct FullAES
{
	double x, y, fill, size;
	string colour, label;
}

struct Plotter
{
	alias Impl = void function(ref GGPlotD gg, const(FullAES)[] data, ref const(Options) options);

	string def_field_spec;
	Impl plot;
	string help_desc;
}

enum PlotType
{
	scatter,
	line,
	box,
	hist,
	hist2,
	density,
	density2,
	labels,
}

enum Tribool
{
	automatic,
	yes,
	no,
}

immutable plotters = [
	Plotter("xyc", &plotScatter, "Scatter plot using x, y and colour."),
	Plotter("xyc", &plotLine, "Line plot using x and y values.  Different colours plotted as separate lines."),
	Plotter("xl", &plotBox, "Box plot of x values.  Use labels to group values into separate plots."),
	Plotter("xl", &plotHist, "Histogram of x values.  Use labels to group values into separate histograms."),
	Plotter("xyl", &plotHist2, "2D histogram of x and y values.  Use labels to group values into separate histograms."),
	Plotter("xc", &plotDensity, "Smooth kernel density plot of x values.  Use colours to separate multiple plots."),
	Plotter("xyc", &plotDensity2, "Smooth 2D kernel density plot of x and y values.  Use colours to separate multiple plots."),
	Plotter("xyl", &plotLabels, "Plot labels at x and y coordinates."),
];

enum Field
{
	x,
	y,
	fill,
	size,
	colour,
	label,
}
immutable field_codes = "xyfscl";

struct Options
{
	@Option("help", "h")
	@Help("display this help and exit")
	OptionFlag help;

	@Option("help-type")
	@Help("display help on plot types and exit")
	OptionFlag help_type;

	@Option("help-fields")
	@Help("display help on specifying fields and exit")
	OptionFlag help_fields;

	@Option("type", "t")
	@Help("plot type (" ~ [__traits(allMembers, PlotType)].join(", ") ~ ")")
	PlotType type = PlotType.scatter;

	@Option("output-file", "o")
	@Help("output file name")
	string out_fname = null;

	@Option("size", "s")
	@Help("output dimensions (WxH)")
	string size = "640x480";

	int width;
	int height;

	void parseSize()
	{
		import std.utf : byCodeUnit;
		try
		{
			auto pieces = size.byCodeUnit.findSplit("x".byCodeUnit);
			enforce(pieces);
			width = pieces[0].to!int;
			height = pieces[2].to!int;
		}
		catch (Exception e)
		{
			throw new Exception(text("Invalid size spec '", size, "'. Expected WxH format (e.g., 640x480)"));
		}
	}

	@Option("delimiter", "d")
	@Help("field delimiter")
	string delim = "\t";

	@Option("fields", "f")
	@Help("field specifier (c = colour, l = label, x = x, y = y)")
	string field_spec = null;
	size_t[Field.max+1] field_idx;

	void parseFieldSpec()
	{
		if (field_spec is null) field_spec = plotters[type].def_field_spec;
		field_idx[] = size_t.max;
		foreach (size_t idx, char c; field_spec)
		{
			import std.utf : byCodeUnit;
			if (c == '.') continue;
			const fi = field_codes.byCodeUnit.countUntil(c);
			enforce(fi != -1, text("Invalid field spec '", field_spec, "'. Unrecognised character: ", c));
			field_idx[cast(Field)fi] = idx;
		}
	}

	@Argument("input files", Multiplicity.zeroOrMore)
	@Help("paths of data files to plot")
	string[] in_fnames;

	@Option("header", "H")
	@Help("treat the first line of each file as a header")
	OptionFlag has_header;

	@Option("title")
	@Help("plot title")
	string title = null;

	@Option("xtitle")
	@Help("x axis title")
	string xtitle = null;

	@Option("ytitle")
	@Help("y axis title")
	string ytitle = null;

	@Option("xrange")
	@Help("x axis range (min,max)")
	string xrange = null;

	@Option("yrange")
	@Help("y axis range (min,max)")
	string yrange = null;

	@Option("add-legend")
	@Help("add legend to plot (" ~ [__traits(allMembers, Tribool)].join(", ") ~ ")")
	Tribool add_legend = Tribool.automatic;
}

int main(string[] args)
{
	if (args.length == 2 && args[1] == "--version")
	{
		writeln(kVersion);
		return 0;
	}

	const usage = usageString!Options(args[0]);
	Options options;
	try
	{
		options = parseArgs!Options(args[1..$]);
		options.parseSize();
		options.parseFieldSpec();
	}
	catch (ArgParseHelp e)
	{
		writeln(usage);
		writeln(helpString!Options());
		return 0;
	}
	catch (Exception e)
	{
		writeln(e.msg);
		writeln(usage);
		return 1;
	}

	if (options.help_type)
	{
		writeln(usage);
		foreach (type; [EnumMembers!PlotType])
		{
			writeln("--type ", type);
			writeln("\t", plotters[type].help_desc);
			writeln("\tDefault field specifier: ", plotters[type].def_field_spec, "\n");
		}
		return 0;
	}

	if (options.help_fields)
	{
		writeln(usage);
		writeln("A field specifier like \".xy\" means the first field in the input is ignored, the second is read as x values, and the third is read as y values.\n\nHere is the list of all codes:");

		foreach (field; [EnumMembers!Field])
		{
			writeln(field_codes[field], " = ", field);
		}
		return 0;
	}

	auto readFile(File file, string fname)
	{
		auto lines = file.byLine;
		if (options.has_header) lines.popFront();
		return lines.map!(l => parseLine(options, fname, l));
	}
	FullAES[] data;
	if (options.in_fnames.empty)
	{
		data = readFile(stdin, "STDIN").array;
	}
	else
	{
		data = options.in_fnames.map!(fn => readFile(File(fn, "rt"), fn)).joiner.array;
	}
	auto gg = GGPlotD();
	plotters[options.type].plot(gg, data, options);
	if (options.title !is null) gg.put(title(options.title));
	if (options.xtitle !is null) gg.put(xaxisLabel(options.xtitle));
	if (options.ytitle !is null) gg.put(yaxisLabel(options.ytitle));

	static void applyRange(string range, void delegate(double,double) applyer)
	{
		import std.utf : byCodeUnit;
		if (range is null) return;
		try
		{
			auto pieces = range.byCodeUnit.findSplit(",".byCodeUnit);
			const lo = pieces[0].to!double;
			const hi = pieces[2].to!double;
			applyer(lo, hi);
		}
		catch (Exception e)
		{
			throw new Exception(text("Invalid range spec '", range, "'.  Expected min,max format (e.g., -1.0,1.0)"));
		}
	}
	applyRange(options.xrange, (lo,hi) { gg.put(xaxisRange(lo, hi)); });
	applyRange(options.yrange, (lo,hi) { gg.put(yaxisRange(lo, hi)); });

	if (options.out_fname is null)
	{
		auto gtkwin = new GTKWindow();
		auto tid = new Thread({ gtkwin.run(args[0], options.width, options.height); }).start();
		gtkwin.draw(gg, options.width, options.height);
		tid.join();
	}
	else
	{
		gg.save(options.out_fname, options.width, options.height);
	}
	return 0;
}

FullAES parseLine(ref const(Options) options, string fname, const(char)[] line)
{
	const fields = line.split(options.delim);
	FullAES ret;
	void extractField(T)(ref T member, Field field, T def_value)
	{
		const idx = options.field_idx[field];
		if (fields.length <= idx)
		{
			member = def_value;
			return;
		}
		member = fields[idx].to!(typeof(member));
	}
	extractField(ret.x, Field.x, 0.0);  // FIXME: increment
	extractField(ret.y, Field.y, 0.0);
	extractField(ret.fill, Field.fill, 0.5);
	extractField(ret.colour, Field.colour, fname);
	extractField(ret.label, Field.label, null);
	return ret;
}

void plotScatter(ref GGPlotD gg, const(FullAES)[] data, ref const(Options) options)
{
	import ggplotd.aes;
	auto d = data.map!(d => aes!("x", "y", "colour")(d.x, d.y, d.colour));
	gg.put(geomPoint(d));
	addLegendIfNeeded(gg, options, data.doesVaryOn!"colour", discreteLegend());
}

void plotLine(ref GGPlotD gg, const(FullAES)[] data, ref const(Options) options)
{
	import ggplotd.aes;
	auto d = data.map!(d => aes!("x", "y", "colour")(d.x, d.y, d.colour));
	gg.put(geomLine(d));
	addLegendIfNeeded(gg, options, data.doesVaryOn!"colour", discreteLegend());
}

void plotBox(ref GGPlotD gg, const(FullAES)[] data, ref const(Options) options)
{
	import ggplotd.aes;
	auto d = data.map!(d => aes!("x", "label")(d.x, d.label));
	gg.put(geomBox(d));
}

void plotHist(ref GGPlotD gg, const(FullAES)[] data, ref const(Options) options)
{
	import ggplotd.aes;
	gg.put(geomHist(data.map!(x => cast(FullAES)x).array));  // FIXME: avoid copy by making geomHist work with const
	addLegendIfNeeded(gg, options, data.doesVaryOn!"label", discreteLegend());
}

void plotHist2(ref GGPlotD gg, const(FullAES)[] data, ref const(Options) options)
{
	import ggplotd.aes;
	gg.put(geomHist2D(data.map!(x => cast(FullAES)x).array));  // FIXME: avoid copy by making geomHist2D work with const
	addLegendIfNeeded(gg, options, data.doesVaryOn!"label", discreteLegend());
}

void plotDensity(ref GGPlotD gg, const(FullAES)[] data, ref const(Options) options)
{
	gg.put(geomDensity(data));
	addLegendIfNeeded(gg, options, data.doesVaryOn!"colour", discreteLegend());
}

void plotDensity2(ref GGPlotD gg, const(FullAES)[] data, ref const(Options) options)
{
	gg.put(geomDensity2D(data));
	addLegendIfNeeded(gg, options, data.doesVaryOn!"colour", discreteLegend());
}

void plotLabels(ref GGPlotD gg, const(FullAES)[] data, ref const(Options) options)
{
	import ggplotd.aes;
	auto d = data.map!(d => aes!("x", "y", "label", "colour")(d.x, d.y, d.label, d.colour));
	// FIXME: The axis ranges need to widen to fit the labels
	gg.put(geomLabel(d));
}

bool doesVaryOn(string field, R)(R r)
{
	return r.map!(e => mixin("e.", field)).uniq.take(2).walkLength > 1;
}

void addLegendIfNeeded(L)(ref GGPlotD gg, ref const(Options) options, lazy bool auto_condition, lazy L legend)
{
	final switch (options.add_legend)
	{
		case Tribool.automatic:
			if (auto_condition) gg.put(legend);
			break;

		case Tribool.yes:
			gg.put(legend);
			break;

		case Tribool.no:
			break;
	}
}

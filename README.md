# `ggplotd-cli`

Command line tool for quickly plotting graphs using [`ggplotd`](https://github.com/BlackEdder/ggplotd).  It's designed to complement the [`tsv-utils` toolset](https://github.com/eBay/tsv-utils).

Features:

* Draw scatter, line, box, histogram (1D and 2D), kernel density (1D and 2D) and word-cloud-style label plots.
* Output to GUI window or to a variety of image file formats (as supported by Cairo backend).

Currently it's an 80% solution: it's useful for a lot of quick data visualisation, but has many limitations.  Please support [the underlying `ggplotd` library](https://github.com/BlackEdder/ggplotd) if you need more features.

## Quick Start

By default, `ggplotd-cli` reads in tab-separated values and draws a scatter graph, using the first column for x values, the second column for y values and (optionally) the third column for tagging different data series.

```
$ # Generate some sin() and cos() data
$ cat sincos.tsv.dj
[: import std.math;
foreach (x; iota(-4.0, 4.0, 0.1)) :]
[= x, "\t", sin(x), "\tsin" ]
[= x, "\t", cos(x), "\tcos" ]
$ djinn sincos.tsv.dj
$ head sincos.tsv
-4	0.756802	sin
-4	-0.653644	cos
-3.9	0.687766	sin
-3.9	-0.725932	cos
-3.8	0.611858	sin
-3.8	-0.790968	cos
-3.7	0.529836	sin
-3.7	-0.8481	cos
-3.6	0.44252	sin
-3.6	-0.896758	cos
$ # Plot to GTK window
$ ggplotd < sincos.tsv 
$ # Plot to PNG image file
$ ggplotd -o sincos.png < sincos.tsv 
```

<img src="sincos.png" alt="Plot of sin() and cos() functions">

## Usage

```
Usage: ggplotd [--help] [--help-type] [--help-fields] [--type=<PlotType>]
               [--output-file=<string>] [--size=<string>]
               [--delimiter=<string>] [--fields=<string>] [--header]
               [--title=<string>] [--xtitle=<string>] [--ytitle=<string>]
               [--xrange=<string>] [--yrange=<string>]
               [--add-legend=<Tribool>] [input files...]

Positional arguments:
 input files     paths of data files to plot

Optional arguments:
 --help, -h      display this help and exit
 --help-type     display help on plot types and exit
 --help-fields   display help on specifying fields and exit
 --type, -t <PlotType>
                 plot type (scatter, line, box, hist, hist2, density, density2,
                 labels)
 --output-file, -o <string>
                 output file name
 --size, -s <string>
                 output dimensions (WxH)
 --delimiter, -d <string>
                 field delimiter
 --fields, -f <string>
                 field specifier (c = colour, l = label, x = x, y = y)
 --header, -H    treat the first line of each file as a header
 --title <string>
                 plot title
 --xtitle <string>
                 x axis title
 --ytitle <string>
                 y axis title
 --xrange <string>
                 x axis range (min,max)
 --yrange <string>
                 y axis range (min,max)
 --add-legend <Tribool>
                 add legend to plot (automatic, yes, no)
```
